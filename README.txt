/*!
 * Ventilation
 * No Copyright - Francesco Maida 2019
 *
 */
/*
variable.js v1 | Add Ventilation
variable.js v2 | Save Local file
variable.js v3 | Add Report
variable.js v3.1 | Add Kids
variable.js v4 ? | Works with API - Available 4 Digit customizable by websmart

Common issue:
Program is not connected with AS400 hence there is no practical way to know if a barcode has a typology.
The issue is still not resolved but works for 99th percentile of the cases.
Additionally, in US/CA warehouse does not scan the original AS400 therefore the error are very limited.
Barcodes are converted without typology by using the AS400 - JDE conversions. See Report/Config in the tool
for the current conversions

------------------------------------------------------------------------------------------------------------
-----------------------------------KNOWN ISSUES :( ---------------------------------------------------------
------------------------------------------------------------------------------------------------------------

Known issue:

192BM16181520ACZ00702
B is a typology or a Kid style?
A research for 192BM16181520ACZ00702 generate error. B is considered as a typology, therefore these research works:
192M16181520ACZ00702   OK
192AM16181520ACZ00702  OK
192CM16181520ACZ00702  OK
192 M16181520ACZ00702  OK
192BM16181520ACZ00702  KO


192MM16181520ACZ00702 ==> generate issue. Statistically, I drop the M, because cases are very rare/

192BBB16181520ACZ00702  ==> generate issue. Statistically, I drop the B, because cases are very rare/


 */
