/*!
 * Ventilation
 * No Copyright - Francesco Maida 2019
 *
 */
/*
variable.js v1 | Add Ventilation
variable.js v2 | Save Local file
variable.js v3 | Add Report
variable.js v3.1 | Add Kids
variable.js v4 ? | Works with API - Available 4 Digit customizable by websmart

Common issue:
Program is not connected with AS400 hence there is no practical way to know if a barcode has a typology.
The issue is still not resolved but works for 99th percentile of the cases.
Additionally, in US/CA warehouse does not scan the original AS400 therefore the error are very limited.
Barcodes are converted without typology by using the AS400 - JDE conversions. See Report/Config in the tool
for the current conversions

------------------------------------------------------------------------------------------------------------
-----------------------------------KNOWN ISSUES :( ---------------------------------------------------------
------------------------------------------------------------------------------------------------------------

Known issue:

192BM16181520ACZ00702
B is a typology or a Kid style?
A research for 192BM16181520ACZ00702 generate error. B is considered as a typology, therefore these research works:
192M16181520ACZ00702   OK
192AM16181520ACZ00702  OK
192CM16181520ACZ00702  OK
192 M16181520ACZ00702  OK
192BM16181520ACZ00702  KO


192MM16181520ACZ00702 ==> generate issue. Statistically, I drop the M, because cases are very rare/

192BBB16181520ACZ00702  ==> generate issue. Statistically, I drop the B, because cases are very rare/


 */







var touchEnabled = 'ontouchstart' in window;
var _TOUCH_ = (touchEnabled) ? 'touchstart' : 'click';

var app;
var Controller = function Controller(){

        //this.typologySetting=['yes','no','warning'];
        this.data = [];

        this.barcodeFound =[];
        this.barcodeMissing =[];

        this.checkTypology = false;

        this.typology = [];
        this.typology[0] = ['*','7','8','A','B','C','D','E','F','G','H','I','K','L','M','P','Q','R','S','U','W','X','Y','Z'];
        this.typology[1] = ['','','','','','','','','','','','X','X','','','','X','','','','','X','X',''];
        this.storageActive =false;
        this.reset = false;

        this.lastVentilation = '';

        //accepted typology
        /* M modello , B Bambina, G Bambino, X MTM*/
        this.digit4 = ['M','B','G'];

        this.current_user= 'fmaida';
        this.filename= 'order.json';



        this.returnus = {
            active:true,
            filename:[],
            reset:false,
            reset_sentences:['Insert Return US Document for Ventilation','Insert the PIN to complete the reset']
        };


        this.path ='JSON';
        this.orderList=[];
        this.priorityList=[];

        this.previousBarcode = [];

        this.category=[];
        this.categoryCount=[];

        this.init = function(){
            Object.keys(getData()).length === 0 ? this.getDataFromJson() : this.loadDataLocal()
        };

        this.init();
};


Controller.prototype.clearSpace = function(){};
Controller.prototype.assign = function(obj){
    this.data = Normalize(obj);
    this.getTypology();
    this.pivotData();
    this.uniqueCategory();
    this.saveData(true)
};

Controller.prototype.uniqueCategory = function(){
    var _t=this;
    this.data.map(function(item,pos,array){
            _t['category'].indexOf(item['reference'])==-1 && _t['category'].push(item['reference']);
        }
    )
    this.updateStat();
};

Controller.prototype.updateStat = function(){

    var _t = this;
    this.category.map(function(ii,p,a) {

        _t['categoryCount'][p] = (_t.data.filter(function (i, p, a) {
            return i['reference'] === ii
        })).reduce(function (accumulator, currentValue, currentIndex, array) {
                return accumulator + currentValue['count'];
            }, 0);
    });

};






Controller.prototype.getTypology = function(){

    this.data.map(function(item,pos,array){
        var type = item['barcode'][3];



        item['tipologia'] = this.typologyConversion(type);


        if (type === item['barcode'][4]) {
            //case 0: double typology like "192BB4675519BCD24801" or "192MM4675519BCD24801"
            // compromise ==> No typology or do not consider typology
            item['tipologia'] = '';
            item['notype_barcode']=item['barcode']
        }

        else if (type !== 'M' && this.digit4.indexOf(type)>=0 && this.digit4.indexOf(item['barcode'][4])>=0){
            //case 1: double typology like "192MM4675519BCD24801"
            //case 2: typology that is also recognized as an OK 4 digit



           //remove 3rd digit
            //item['notype_barcode'] =item['barcode']
           var _arr = item['barcode'].split('')
           var removed = _arr.splice(3,1);
            //item['notype_barcode'] = c.split('').splice(3,3).join();
            item['barcode']= item['notype_barcode'] = _arr.join('')




        }
        else{
            //case 3: typology that is not a valid 4 digit
            //case default: no typology at all

            item['notype_barcode'] = this.digit4.indexOf(type)>=0? item['barcode']:item['barcode'].replace(type,'');
        }




        //item['notype_barcode'][3] !='M' &&  item['notype_barcode'].split('').splice(item['notype_barcode'].indexOf(item['notype_barcode'][3]),1).join('')
        //item['notype_barcode'][3] !='M' && item['notype_barcode']
        //item['notype_barcode'][3] !='M' && console.log(item['notype_barcode'].split('').splice(item['notype_barcode'].indexOf(item['notype_barcode'][3]),1).join(''))





},this)

}

Controller.prototype.pivotData = function(){
    this.data=pivotTable(this.data)

    this.data.map(function(i,p,a){
        i["count"]=i['quantity']
    })
};

Controller.prototype.saveData = function(initial){
    var i=0,_obj = {};
    _obj.data = this.data;
    _obj.time = todayDate();

    if (Object.keys(getData()).length === 0 && initial){
       // localStorage.setItem("VENTILATION", JSON.stringify(_obj))
        console.log("Local storage VUOTA all'inizio")
    } else {console.log('non salvare local storage')}

    if (!initial){
        localStorage.setItem("VENTILATION", JSON.stringify(_obj))
        console.log("local storage salvata da avanzamento utente")
    }





    //localStorage.setItem("VENTILATION", JSON.stringify(_obj));

};

Controller.prototype.loadDataLocal = function(){
    var _obj =getData();
    alert("Previous Ventilation performed on "+_obj.time+" recovered");
    this.lastVentilation = _obj.time;
    this.updateData(_obj.data);
    this.uniqueCategory();
    updateLabel(_obj.time);

};

Controller.prototype.updateData = function(obj) {
    this.data = obj;
};


Controller.prototype.cancelDataLocal = function(){
    localStorage.removeItem("VENTILATION");

    setTimeout(function(){location.reload();},1000);

};



/*BARCODE RULE CONVERSION*/
/*4°digit	Descr.	Nuovo 4° digit
*	FALLATO RECUPERATO
7	RIMANENZE PRODUZIONE
8	ORDINE NEGOZIO
A	MODELLO/COLORE ANNULLATO
B	BASICI
C	CONTINUATIVI
D	PRODUZIONE O CLIENTE 6003
F	FALLATO
G	PANTAL.FALLATI TESSUTO LAVATO
H	3° SCELTA USA (AC.MALEICO 112)
I	CAPI INDOSSATI PER CAMPIONARIO	X
K	FALLATI CAMPIONARIO	X
L	PROVE FILATO NON INDOSSATO
M	CAMPIONARIO REC. SOLO X MONO	X
P	PROTOTIPI
Q	RIMANENZE CAMPIONARIO	X
R	RECUPERABILI
S	PROD NON CONFORME E PRIMI CAPI
U	SCARPE INDOSSATE FALLATE
W	RESO NON PRODOTTO AVANZ.PRODUZ
X	CAMPIONARIO	X
Y	CAMPIONARIO NON CONFORME	X
Z	SCARPE FALLATE TZBOH 13-2
*/
Controller.prototype.typologyConversion = function(str){
        return this.typology[1][this.typology[0].indexOf(str)];
}




Controller.prototype.checkBarcode = function(_b){



    var spedition = "";

    if (_b[0] == 0 && _b[1] == 0 && _b[2] == 0){
        return 'serial'
    }

    for (var i = 0,ii= this.data.length; i<ii;i++ ) {
        //console.log(i)

        var barcode = this.checkTypology ? this.data[i]["barcode"]:this.data[i]["notype_barcode"];



        if (barcode === barcodeNormalization(_b,this.checkTypology,this.digit4) && this.data[i]["count"]>=1){
            this.data[i]["count"]--;
            this.saveData();
            this.previousBarcode.push([barcode,this.data[i]["reference"]]);
            this.categoryCount[this.category.indexOf(this.data[i]["reference"])]--;
            return [true,this.data[i]["reference"]]

        }
    }

        //console.log("here");
        this.previousBarcode.push([barcodeNormalization(_b,this.checkTypology,this.digit4),"Item not on the list"]);
        return false;

};




Controller.prototype.getDataFromJson = function(){
   getJSON(this.path+'/'+this.filename,this);
    alert("No previous data stored. Ventilation is starting from scratch")
};

Controller.prototype.cancelData=function(){
    var r = confirm("Do you want to reset the current ventilation_feature? - data will be permanently deleted");
    r ? this.cancelDataLocal():'';

}



//add missing barcode funzion
Controller.prototype.missingBarcode = function(){
    // return this.data.map(function(item,pos,array) {
    //    return item['quantity'] =! 0
    //});

    return this.data.filter(function(e){return e.count >0})

};

//add missing barcode funzion
Controller.prototype.foundBarcode = function(){
    return this.data.filter(function(e){return e.count ==0})
};

Controller.prototype.updateBarcodeStatus = function(){
    this.barcodeFound = this.foundBarcode();
    this.barcodeMissing = this.missingBarcode();
};







