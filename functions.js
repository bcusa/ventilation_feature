function LN(param){return Math.log(param)}
function EXP(param){return Math.exp(param)}
var t;

function getJSON(path,obj){


   /* $.ajax({
        url: 'https://websmart.brunellocucinelli.it/labels/labels/documentdetail/publicread',
        type: 'POST',
        data: {
           'documentId':'2019911200177',
           'documentType':'SHIPPING',
            'username': 'temera',   //If your header name has spaces or any other char not appropriate
            'password': 'temera.2018'
        },
        headers: {
            'username': 'temera',   //If your header name has spaces or any other char not appropriate
            'password': 'temera.2018'  //for object property name, use quoted notation shown in second
        },

        success: function (data) {
           // obj.assign(data);
           console.log(data);
        }
    });*/



    $.ajax({
        url: path,
        dataType: 'json',
        success: function (data) {
            obj.assign(data);
        }
    });
}

//"JSON/order.json"



function Normalize(a){
    return a.map(function(item,index,array){
        item['barcode'] = item['barcode'].toUpperCase().split(' ').join('');
        //verificare regola
        item['count'] = item['quantity'];
        return item;
    })
}

function UniqueCategory(a){

};



function barcodeNormalization(b,typology,fourthDigit){
    if (typology)
        return b.toUpperCase().split(' ').join('');
    else {
      //  console.log(fourthDigit.indexOf(b[3].toUpperCase()))
        b= b.toUpperCase();
        return fourthDigit.indexOf(b[3].toUpperCase()) >=0 ? b.toUpperCase().split(' ').join(''):
            b.toUpperCase().replace(b[3],'').split(' ').join('');


       /* return b[3].toUpperCase() == 'M' ? b.toUpperCase().split(' ').join(''):
            b.toUpperCase().replace(b[3],'').split(' ').join('');*/
    }
        //161XM -----

}


function updateLabel(t){
    $("#last").text("You are working on ventilation_feature started on " + t)
}


function updateReference(v,value){
    v ? v ==='serial' ? $("#sort").removeClass().addClass('gold').text("Warning: SERIAL SCANNED"):$("#sort").removeClass().addClass('success').text(v[1]):$("#sort").removeClass().addClass('fail').text('ITEM NOT IN THE LIST')
    v ? v ==='serial' ? $("#sort2").removeClass().text(''):$("#sort2").removeClass().addClass('success').text(value ):$("#sort2").removeClass().addClass('fail').text(value)
    //$("#selection").text(value)
    app.previousBarcode.length>1 && $("#previous").removeClass().addClass('active').text("Barcode " + app.previousBarcode[app.previousBarcode.length-2][0] + " for " +  app.previousBarcode[app.previousBarcode.length-2][1])
    updateLiveStatistic();

}

function noValidBarcode(v){
   // $("#selection").text(v)
    $("#sort").removeClass().addClass('fail').text('Not valid barcode !');
    $("#sort2").removeClass().text('')
    app.previousBarcode.length>1 && $("#previous").removeClass().addClass('active').text("Barcode " + app.previousBarcode[app.previousBarcode.length-2][0] + " for " +  app.previousBarcode[app.previousBarcode.length-2][1])

}

function pivotTable(a) {

    for (var i = 0,ii= a.length,j=0; i<ii;i++ ){
        var c = a[i]["quantity"];

        for (j = i+1;j<ii;j++){
           if(a[i]['barcode'] === a[j]['barcode'] && a[i]['reference'] === a[j]['reference'] && !a[j]['d']){
               a[i]["quantity"] += a[j]["quantity"];
               a[j]['d'] = true;
           }
        }

    }

    return a.filter(function(i,p,a){return i['d']!=1})

}


function pivotTable(a) {

    for (var i = 0,ii= a.length,j=0; i<ii;i++ ){
        var c = a[i]["quantity"];

        for (j = i+1;j<ii;j++){
            if(a[i]['barcode'] === a[j]['barcode'] && a[i]['reference'] === a[j]['reference'] && !a[j]['d']){
                a[i]["quantity"] += a[j]["quantity"];
                a[j]['d'] = true;
            }
        }

    }

    return a.filter(function(i,p,a){return i['d']!=1})

}





function loadLiveStat(cat,catcount){
    //$livestat
    $livestat.empty();
    var $cat='',$catcount ='';
    cat.map(function(i,p,a){



        $cat = catcount[p]!==0 ? "<div class='stat category "+i+"'>"+i+"</div>" : "<div class='complete stat category "+i+"'>"+i+"</div>"
        $catcount = catcount[p]!==0 ? "<div class='stat catcount "+i+"'>"+catcount[p]+"</div>" :
            "<div class='complete stat catcount "+i+"'>DONE</div>";

        $livestat.append($cat);
        $livestat.append($catcount)
    });

    var cc = catcount.reduce(function (accumulator, currentValue, currentIndex, array) {
        return accumulator + currentValue;
    }, 0);

    $('.globalcount').html(cc>0?cc:'Cartonization COMPLETE')
}

function updateLiveStatistic(){


    var globalcount =0;
    app.category.map(function(i,p,a){

        var $cat = $('.catcount.'+ i.split(' ').join('.'));
        var $header = $('.'+ i.split(' ').join('.'));

        //console.log('.catcount.'+ i.split(' ').join('.'));

        //console.log(app.categoryCount[p] !=  $cat.html())
        //console.log($cat.length)
        globalcount+=app.categoryCount[p];

        if (app.categoryCount[p] !=  $cat.html()){



            $cat.html(
                app.categoryCount[p]
            );

            $header.addClass('found');

            app.categoryCount[p] === 0 && $header.addClass('complete');
            app.categoryCount[p] === 0 && $cat.html('DONE');


        }

        setTimeout(function(){$('.found').removeClass('found')},500)
    });

    $('.globalcount').html(globalcount >0 ? globalcount:'Cartonization Complete');
}









function populateChechbox(value,id) {
    var options
    for (var i = 0; i < value.length; i++) {
        options += '<option value="' + value[i] + '">' + value[i] + '</option>';
    }
    id.append(options)
}


/*function filter(value, index, self) {
    return self.indexOf(value) === index;
}*/

function unbindPopup(){
    $('#discard').off();
    $('#fname').off();
    $('.popup-container').hide();
    $('#okbutton').addClass('disabled').off();
    $('#fname').val('');
}


//load ventilation from RETURN US AJAX

/*function loadReturnVentilation(){
    $('.popup-container-return').show();
}*/


function enablePopup(){
    $('.popup-container').show();

    $('#discard').on('click',function(){
        unbindPopup();
        $('.popup-container').hide();
    })

    $('#fname').on( "keyup", function(){
        $('#okbutton').addClass('disabled').off();
        if ($(this)[0].value == 'cucinelli' || $(this)[0].value == '0000'){
            $('#okbutton').removeClass('disabled')
            $('#okbutton').on('click',function(){
                unbindPopup();
                app.cancelData();
            });
        }

    })

    $('#fname').on( "keypress", function(e){
        if (e.keyCode == 13) {
            if ($(this)[0].value == 'cucinelli' || $(this)[0].value == '0000'){
                    unbindPopup();
                    app.cancelData();
            }
        }
    })







}






function onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
}

Array.prototype.unique = function() {
    return this.filter(function (value, index, self) {
        return self.indexOf(value) === index;
    });
}

function getData(){
    var i = 0,
        _obj = {},
        _sKey;
    for (; _sKey = window.localStorage.key(i); i++) {
        if (!_sKey.indexOf('VENTILATION')) _obj =JSON.parse(window.localStorage.getItem(_sKey));
    }
    return _obj;
}


function todayDate(){
    var objToday = new Date(),
        weekday = new Array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'),
        dayOfWeek = weekday[objToday.getDay()],
        domEnder = function() { var a = objToday; if (/1/.test(parseInt((a + "").charAt(0)))) return "th"; a = parseInt((a + "").charAt(1)); return 1 == a ? "st" : 2 == a ? "nd" : 3 == a ? "rd" : "th" }(),
        dayOfMonth =  ( objToday.getDate() < 10) ? '0' + objToday.getDate() + domEnder : objToday.getDate() + domEnder,
        months = new Array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'),
        curMonth = months[objToday.getMonth()],
        curYear = objToday.getFullYear(),
        curHour = objToday.getHours() > 12 ? objToday.getHours() - 12 : (objToday.getHours() < 10 ? "0" + objToday.getHours() : objToday.getHours()),
        curMinute = objToday.getMinutes() < 10 ? "0" + objToday.getMinutes() : objToday.getMinutes(),
        curSeconds = objToday.getSeconds() < 10 ? "0" + objToday.getSeconds() : objToday.getSeconds(),
        curMeridiem = objToday.getHours() > 12 ? "PM" : "AM";
    return curHour + ":" + curMinute + "." + curSeconds + curMeridiem + " " + dayOfWeek + " " + dayOfMonth + " of " + curMonth + ", " + curYear;


}