var options
(function(global,jquery){
    global.$back = jquery('.back');
    global.$missing = jquery('#missing');
    global.$found = jquery('#found');
    global.$destination = jquery('#destination');
    global.$season = jquery('#season');
    global.$reset = jquery('#reset');
    global.$sku = jquery('#sku');
    global.app = new Controller();

    global.report = {
        reference:[],
        season:[]
    };

    global.filter = {
        season:'default',
        reference:'default',
        missing:1,
        found:1,
        sku:''
    };




   $back.on('click',returnventilation);
   $reset.on('click',reload);

    setTimeout(function(){
        app.updateBarcodeStatus();
        populateData();
        createLayout('missing','found',app.barcodeMissing,app.barcodeFound);}
    ,200)



})(window,$);

function returnventilation(){
   window.location.replace('index.html');
}

function reload(){
    window.location.replace('report.html');
}





var dyi
function populateData(){


    app.data.map(function(item,pos,array){

        report.reference.push(item['reference']);
        report.season.push(item['barcode'].substring(0,3));
    });


    report.reference =  report.reference.filter(onlyUnique);
    report.season = report.season.filter(onlyUnique);
    // console.log(barcode)

    populateChechbox(report.reference,$destination);
    populateChechbox(report.season,$season);



    //bind
    //$destination.on('change',selectDestination);

    $season.on('change',function(){
        filter.season =  $(this).val();
        //$('#result').text('');
        applyFilter();
    });

    $destination.on('change',function(){
        filter.reference =  $(this).val();
        applyFilter();
    });


    $( "[type=checkbox]").on('click',function(){
        filter[this.id] = ($(this)[0].checked);
        applyFilter();
    })

    $season.on('change',function(){
        filter.season =  $(this).val();
        //$('#result').text('');
        applyFilter();
    });

    $sku.on('keyup',function(){
        filter[this.id] = ($(this).val()).toUpperCase().trim();
        applyFilter();
        //$('#result').text('');
        //applyFilter();
    });


}


function applyFilter(){

    console.log(filter)

    //$('#result').text('');

    app.updateBarcodeStatus();

    //APPLY FILTER

    //SEASON
  //  if (filter.season !== 'default' && filter.found && app.barcodeFound) app.barcodeFound = app.barcodeFound.filter(function(e){return e.barcode.substring(0,3) ==filter.season});



   if ( filter.season != 'default') app.barcodeFound =  app.barcodeFound.filter(function(e){return e.barcode.substring(0,3) ==filter.season});
   if ( filter.season != 'default') app.barcodeMissing =  app.barcodeMissing.filter(function(e){return e.barcode.substring(0,3) ==filter.season});



    //Reference


    if (filter.reference !== 'default') app.barcodeFound=  app.barcodeFound.filter(function(e){return e.reference ==filter.reference});
    if (filter.reference !== 'default') app.barcodeMissing= app.barcodeMissing.filter(function(e){return e.reference ==filter.reference});


    if (filter.sku) app.barcodeFound=  app.barcodeFound.filter(function(e){return e.barcode.indexOf(filter.sku)>=0});
    if (filter.sku) app.barcodeMissing= app.barcodeMissing.filter(function(e){return e.barcode.indexOf(filter.sku)>=0});



    // DATA TO SHOW

        filter.found && filter.missing  && createLayout(1,1,app.barcodeMissing,app.barcodeFound);
        filter.found !=true && filter.missing  && createLayout(1,0,app.barcodeMissing,app.barcodeFound);
        filter.found && filter.missing!=true  && createLayout(0,1,app.barcodeMissing,app.barcodeFound);

    //
}


var debug
function createLayout(missing,found,arrMissing,arrFound){


    console.log(arguments)

    var html = '<table id="ventilation"><thead></thead><th>REFERENCE</th><th>BARCODE</th><th>EXPECTED</th><th>FOUND</th><tbody>';

    if (missing) {
        arrMissing.map(function (item, pos, array) {
            html += '<tr class="missing">';
            html += '<td>' + item['reference'] + '</td>';
            html += '<td>' + item['barcode'] + '</td>';
            html += '<td>' + item['quantity'] + '</td>';
            html += '<td>' + (+item['quantity']-item['count']) + '</td>';
            html += '</tr>';
        });
    }

    if (found) {
        arrFound.map(function (item, pos, array) {
            html += '<tr class="found">';
            html += '<td>' + item['reference'] + '</td>';
            html += '<td>' + item['barcode'] + '</td>';
            html += '<td>' + item['quantity'] + '</td>';
            html += '<td>' + item['quantity'] + '</td>';
            html += '</tr>';
        });
    }

    html += '</tbody><tfoot><tr></tr></tfoot></table>';

    //console.log(html)

    $('#result').empty();
    $(html).appendTo('#result');



   // $(html).appendTo('#result');
}



